package formative4;

public class Formative3 {
    public static void main(String[] args) {
        int tambah = 0;
        for (int i = 0; i < args.length; i++){
            try {
                tambah += Integer.parseInt(args[i]);
            }catch (NumberFormatException e){
                System.out.println(e.getMessage());
            }
        }
        System.out.println("hasil keseluruhan = " + tambah);
    }
}
