package formative4;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Formative2 {
    public static void main(String[] args) {
        int i = 1;
        Path currentPath = Paths.get("");
        String path = currentPath.toAbsolutePath().toString();

        while (i <= 10){
            try{
                String namaFile = "form"+i+".txt";
                FileInputStream fileInputStream = new FileInputStream(path+"/form1/"+namaFile);
                byte[] bytes = fileInputStream.readAllBytes();
                String str = new String(bytes);

                System.out.println(namaFile);
                System.out.println(str);
                System.out.println("");
                fileInputStream.close();

            }catch (Exception e){
                System.out.println(e);
            }finally {
                i++;
            }
        }
    }
}
