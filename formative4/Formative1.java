package formative4;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Formative1 {
    public static void main(String[] args) {
        int i = 1;
        Path currentPath = Paths.get("");
        String path = currentPath.toAbsolutePath().toString();

        try {
            System.out.println("Path kita adalah : " + path);
            Path direktori = Paths.get(path + "/form1/");
            Files.createDirectories(direktori);
            System.out.println("Direktori sudah terbuat");
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (i <= 10) {
            FileOutputStream fout = null;
            File file = null;
            try {

                file = new File(path + "/form1/form" + i + ".txt");

                fout = new FileOutputStream(file);
                String text = "Hello java " + i;
                if (!file.exists()) {
                    file.createNewFile();
                }

                byte[] b = text.getBytes();

                fout.write(b);
                fout.close();
                System.out.println("File sedang di buat");
            } catch (Exception e) {
                System.err.println("Failed to create file!" + e.getMessage());
            } finally {
                i++;
            }
        }
    }
}
